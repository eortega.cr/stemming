#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt

path = os.path.dirname(os.path.realpath(__file__))
path_file = os.path.join(path, "resultados.csv")
vocabulary = os.path.join(path, "Vocabulario.txt")
words = ["escolar", "escuela", "escuelas", "estado", "escuelilla", "prescolar"]
# words = ["escolar", "escuela", "escuelas", "estado", "escuelilla", "prescolar", "comiendo, comer", "funcion", "funcionando", "ir", "casa", "comida", "comunicacion", "volver", "volviendo", "volar"]
matrix = []


def read_file():
    print("reading vocabulary")
    global words
    with open(vocabulary, 'r', encoding="utf-8") as f:
        words = [line.rstrip() for line in f]


def n_grams(word, n):
    gram = []
    index = 0
    for _ in word[:len(word) - n + 1]:
        gram.append(word[index:index + n])
        index = index + 1
    return gram


def create_grams(n):
    # read_file()
    print("creating grams")
    for i, word_a in enumerate(words):
        matrix.append([])
        gram_a = n_grams(word_a, n)
        unique_gram_a = set(gram_a)
        for word_b in words:
            gram_b = n_grams(word_b, n)
            unique_gram_b = set(gram_b)
            shared_gram = unique_gram_a.intersection(unique_gram_b)
            similarity_measure = (2 * len(shared_gram)) / (len(unique_gram_a) + len(unique_gram_b))
            matrix[i].append(similarity_measure)
    print("writing")
    fh = open(path_file, "w", encoding="utf-8")
    for row in matrix:
        line = ';'.join([str(round(elem, 4)) for elem in row])
        print(line)
        fh.writelines(line)
        fh.write("\n")
    fh.close()


def slink(x_matrix):
    print("SLINK")
    plt.imshow(x_matrix)
    plt.colorbar()
    plt.show()

    z = linkage(x_matrix, 'single')
    plt.figure(figsize=(25, 10))
    dendrogram(z, labels=words)
    plt.show()


create_grams(2)
slink(matrix)