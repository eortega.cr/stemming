#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


def es_vocal(c):
    if c == 'a' or c == 'e' or c == 'i' or c == 'o' or c == 'u' or c == 'á' or c == 'é' or c == 'í' or c == 'ó' or c == 'ú':
        return True
    return False


def posVocal(palabra, comienzo):
    tamano = len(palabra)
    for i in range (comienzo,tamano):
        if es_vocal(palabra[i]):
            return i
    return tamano


def posConsonante(palabra, comienzo):
    tamano = len(palabra)
    for i in range (comienzo,tamano):
        if not es_vocal(palabra[i]):
            return i
    return tamano


def detSuf(palabra, sufijo):
    if len(palabra) < len(sufijo):
        return False
    if substr(palabra, -len(sufijo)) == sufijo:
        return True
    return False


def sufijoMasLargo(palabra, sufijos):
    temp = ""
    for i in range(0,len(sufijos)):
        if detSuf(palabra, sufijos[i]):
            if len(sufijos[i]) >= len(temp):
                temp = sufijos[i]
    return temp


def quitarAcentos(palabra):
    palabra = palabra.replace('á', 'a')
    palabra = palabra.replace('é', 'e')
    palabra = palabra.replace('í', 'i')
    palabra = palabra.replace('ó', 'o')
    palabra = palabra.replace('ú', 'u')
    return palabra


def substre (palabra, empieza, tamano):
    if empieza == tamano:
        return ""
    else:
        if empieza >= 0:
            if tamano >= 0:
                termina = empieza + tamano
                if termina > len(palabra):
                    palabra = concatena(palabra, empieza, tamano)
                    return palabra
                else:
                    palabra = concatena(palabra, empieza, tamano)
                    return palabra
            else:
                termina = len(palabra) + tamano
                try:
                    palabra = concatena(palabra, empieza, termina)
                except Exception as ex:
                    print(ex)
                    palabra = ""
                return palabra
        else:
            if tamano >= 0:
                nuevoComienzo = len(palabra) + empieza
                termina = nuevoComienzo + tamano
                if (termina > len(palabra)):
                    palabra = concatena(palabra, nuevoComienzo, len(palabra))
                    return palabra
                else:
                    palabra = concatena(palabra, nuevoComienzo, termina)
                    return palabra
            else:
                nuevoComienzo = len(palabra) + empieza
                termina = len(palabra)+tamano

                try:
                    palabra = concatena(palabra, nuevoComienzo, termina)
                except Exception as ex:
                    print(ex)
                    palabra = ""

                return palabra


def concatena(antigua,empieza, termina):
    palabra = ""
    for i in range (empieza, termina):
        palabra = palabra + antigua[i]
    return palabra

def substr(palabra, empieza):
    if abs(empieza) > len(palabra):
        return palabra
    elif empieza >= 0:
        return concatena(palabra, empieza, len(palabra))
    else:
        return concatena(palabra, len(palabra) + empieza, len(palabra))


def stemm(palabra):
    tamano = len(palabra)
    if tamano <= 2:
        return quitarAcentos(palabra)

    palabra = palabra.lower()
    r1 = r2 = rv = tamano

    i = 0
    while i < (tamano - 1) and r1 == tamano:
        if (es_vocal(palabra[i])) and (es_vocal(palabra[i + 1]) is False):
            r1 = i + 2
        i = i+1

    j = r1

    while j < (tamano-1) and r2 == tamano:
        if (es_vocal(palabra[j])) and (es_vocal(palabra[j+1]) is False):
            r2 = j+2
        j = j+1

    if tamano > 3:
        if not es_vocal(palabra[1]):
            rv = posVocal(palabra, 2)
        elif es_vocal(palabra[0]) and es_vocal(palabra[1]):
            rv = posVocal(palabra, 2)+1
        else:
            rv = 3

    r1_text = substr(palabra, r1)
    r2_text = substr(palabra, r2)
    rv_text = substr(palabra, rv)

    palabraOriginal = palabra

    pronombre_suf = ["me", "se", "sela", "selo", "selas", "selos", "la", "le", "lo", "las", "les", "los", "nos"]
    pronombre_suf_pre1 = ["iéndo", "ándo", "ár", "ér", "ír"]
    pronombre_suf_pre2 = ["ando", "iendo", "ar", "er", "ir"]

    suf = sufijoMasLargo(palabra, pronombre_suf)

    if not suf == "":
        pre_suff = sufijoMasLargo(substr(rv_text,0,-len(suf)), pronombre_suf_pre1)

        if not pre_suff == "":
            palabra = quitarAcentos(substr(palabra,0,-len(suf)))
        else:
            pre_suff = sufijoMasLargo(substr(rv_text,0,-len(suf)), pronombre_suf_pre2)
            if not pre_suff == "" or (detSuf(palabra, "yendo") and (substr(palabra, -len(suf -6), 1) == "u")):
                palabra = substr(palabra, 0, -len(suf))

    if not palabra == palabraOriginal:
        r1_text = substr(palabra, r1)
        r2_text = substr(palabra, r2)
        rv_text = substr(palabra, rv)

    palabraDespues = palabra

    suf = sufijoMasLargo(r2_text, ["anza", "anzas", "ico", "ica",
                                   "icos", "icas", "ismo", "ismos", "able", "ables", "ible",
                                   "ibles", "ista", "istas", "oso", "osa", "osos", "osas",
                                   "amiento", "amientos", "imiento", "imientos"])
    if not  suf == "" :
        palabra = substr(palabra, 0, -len(suf))

    suf = sufijoMasLargo(r2_text, ["icadora", "icador",
                                   "icación", "icadoras", "icadores", "icaciones", "icante",
                                   "icantes", "icancia", "icancias", "adora", "ador", "ación",
                                   "adoras", "adores", "aciones", "ante", "antes", "ancia", "ancias"])
    if not suf == "":
        palabra = substr(palabra, 0, len(suf))

    suf = sufijoMasLargo(r2_text, ["logía", "logías"])

    if not suf == "":
        palabra = substr(palabra, 0, -len(suf)) + "log"

    suf = sufijoMasLargo(r2_text, ["ución", "uciones"])

    if not suf == "":
        palabra = substr(palabra, 0, -len(suf))+"u"

    suf = sufijoMasLargo(r2_text, ["encia", "encias"])
    if not suf == "":
        palabra = substr(palabra, 0, -len(suf))+"ente"

    suf = sufijoMasLargo(r2_text, ["ativamente", "ivamente",
"osamente", "icamente", "adamente"])
    if not suf == "":
        palabra = substr(palabra, 0, -len(suf))

    suf = sufijoMasLargo(r2_text, ["amente"])
    if not suf == "":
        palabra = substr(palabra, 0, -len(suf))

    suf = sufijoMasLargo(r2_text, ["antemente", "ablemente","iblemente", "mente"])
    if not suf == "":
        palabra = substr(palabra, 0, -len(suf))

    suf = sufijoMasLargo(r2_text, ["abilidad", "abilidades","icidad", "icidades", "ividad", "ividades", "idad", "idades"])
    if not suf =="":
        palabra = substr(palabra, 0, -len(suf))

    suf = sufijoMasLargo(r2_text, ["ativa", "ativo", "ativas","ativos", "iva", "ivo", "ivas", "ivos"])
    if not suf == "":
        palabra = substr(palabra, 0, len(suf))

    if not palabra == palabraDespues:
        r1_txt = substr(palabra, r1)
        r2_txt = substr(palabra, r2)
        rv_txt = substr(palabra, rv)

    palabraDespues1 = palabra

    if not palabraDespues == palabraDespues1:
        suf = sufijoMasLargo(rv_text, ["ya", "ye", "yan", "yen", "yeron", "yendo", "yo","yó", "yas", "yes", "yais", "yamos"] )
        temporal = substr(palabra, -len(suf) - 1, 1)
        if (not suf == "") and temporal == "u":
            palabra = substr(palabra, 0, -len(suf))

    if not palabra == palabraDespues1:
        r1_txt = substr(palabra, r1)
        r2_txt = substr(palabra, r2)
        rv_txt = substr(palabra, rv)

    palabraDespues2 = palabra

    if palabraDespues2 == palabraDespues1:
        suf = sufijoMasLargo(rv_text, ["arían", "arías",
                        "arán", "arás", "aríais", "aría", "aréis", "aríamos",
                        "aremos", "ará", "aré", "erían", "erías", "erán",
                        "erás", "eríais", "ería", "eréis", "eríamos", "eremos",
                        "erá", "eré", "irían", "irías", "irán", "irás",
                        "iríais", "iría", "iréis", "iríamos", "iremos", "irá",
                        "iré", "aba", "ada", "ida", "ía", "ara", "iera", "ad",
                        "ed", "id", "ase", "iese", "aste", "iste", "an",
                        "aban", "ían", "aran", "ieran", "asen", "iesen",
                        "aron", "ieron", "ado", "ido", "ando", "iendo", "ió",
                        "ar", "er", "ir", "as", "abas", "adas", "idas", "ías",
                        "aras", "ieras", "ases", "ieses", "ís", "áis", "abais",
                        "íais", "arais", "ierais", "aseis", "ieseis", "asteis",
                        "isteis", "ados", "idos", "amos", "ábamos", "íamos","imos", "áramos", "iéramos", "iésemos", "ásemos"])

        if not suf == "":
            palabra = substre(palabra, 0, -len(suf))

        suf = sufijoMasLargo(rv_text, ["en", "es", "éis", "emos"])
        if not suf == "":
            palabra = substr(palabra, 0, -len(suf))
            if detSuf(palabra, "gu"):
                palabra = substr(palabra, 0, -1)

    r1_text = substr(palabra, r1)
    r2_text = substr(palabra, r2)
    rv_text = substr(palabra, rv)

    suf = sufijoMasLargo(rv_text, ["os", "a", "o", "á", "í", "ó"])
    if not (suf == ""):
        palabra = substre(palabra, 0, -len(suf))

    suf = sufijoMasLargo(rv_text, ["e", "é"])

    if not (suf == ""):
        palabra = substr(palabra, 0, -1)
        rv_text = substr(palabra, rv)
        if detSuf(palabra, "u") and detSuf(palabra, "gu"):
            palabra = substr(palabra, 0, -1)

    quitarAcentos(palabra)
    return palabra


def getIndex(palabra, resultados):
    for i in range(0, len(resultados)):
        if resultados[i] == palabra:
            tmp = i
        else:
            tmp = -1
    return tmp


def abrirArchivos():
    palabras = []
    filename = "palabras.txt"
    file = open(filename, "r", encoding="utf-8")
    for line in file:
        palabras.append(line)
    listaPalabras(palabras)


def listaPalabras(palabras):
    resultados = []
    indice = []
    for i in range (0, len(palabras)):
        ind = 0
        tmp = palabras[i].strip('\n')
        palabra = stemm(tmp)
        if palabra in resultados:
            ind = getIndex(palabra, resultados)
            if ind >= 0:
                indice[ind] = indice[ind] + 1
        else:
            resultados.append(palabra)
            indice.append(1)

    fh = open("resultados.txt", "w", encoding="utf-8")
    for i in range(0, len(resultados)):
        print(resultados[i])
        print(indice[i])
        escribe = resultados[i] + "     " + str(indice[i]) + '\n'
        fh.writelines(escribe)
    fh.close()


abrirArchivos()




